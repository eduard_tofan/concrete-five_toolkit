<?php

return [
    'providers' => [
        'eloquent' => \FiveToolkit\Provider\EloquentServiceProvider::class
    ],
];