<?php

namespace FiveToolkit\Eloquent\Events;

use Illuminate\Container\Container;

class Dispatcher extends \Illuminate\Events\Dispatcher
{
    /**
     * Create a new event dispatcher instance.
     * We've overridden this to accept a Container which
     * does not implement the contract interface.
     *
     * @param $container
     */
    public function __construct($container = null)
    {
        $this->container = $container ?: new Container;
    }
}