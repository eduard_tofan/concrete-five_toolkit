<?php

namespace FiveToolkit\Eloquent\Database;

use Illuminate\Support\Fluent;
use Illuminate\Database\DatabaseManager;
use Illuminate\Contracts\Container\Container;

class Manager extends \Illuminate\Database\Capsule\Manager
{
    /**
     * Create a new database capsule manager.
     */
    public function __construct()
    {
        $this->setupContainer(null, new \Illuminate\Container\Container());

        // Once we have the container setup, we will setup the default configuration
        // options in the container "config" binding. This will make the database
        // manager work correctly out of the box without extreme configuration.
        $this->setupDefaultConfiguration();

        $this->setupManager();
    }

    /**
     * Setup the IoC container instance.
     * We've overridden this to accept a Container which
     * does not implement the contract interface.
     *
     * @param Container $container
     * @param null $oldContainer
     */
    protected function setupContainer(Container $container = null, $oldContainer = null)
    {
        $this->container = $oldContainer;

        if (! $this->container->bound('config')) {
            $this->container->instance('config', new Fluent);
        }
    }

    /**
     * Build the database manager instance.
     * We've overridden this to accept a Container which
     * does not implement the contract interface.
     *
     * @return void
     */
    protected function setupManager()
    {
        $factory = new ConnectionFactory($this->container);

        $this->manager = new DatabaseManager($this->container, $factory);
    }
}