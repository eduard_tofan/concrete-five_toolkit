<?php

namespace FiveToolkit\Eloquent\Database;

class ConnectionFactory extends \Illuminate\Database\Connectors\ConnectionFactory
{
    /**
     * Create a new connection factory instance.
     * We've overridden this to accept a Container which
     * does not implement the contract interface.
     *
     * @param $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }
}