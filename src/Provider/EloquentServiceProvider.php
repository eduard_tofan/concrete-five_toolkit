<?php

namespace FiveToolkit\Provider;

use Core;
use Events;
use Exception;
use FiveToolkit\Eloquent\Events\Dispatcher;
use Concrete\Core\Foundation\ClassAliasList;
use FiveToolkit\Eloquent\Database\Manager as DatabaseManager;
use Concrete\Core\Foundation\Service\Provider as ServiceProvider;

class EloquentServiceProvider extends ServiceProvider
{
    /**
     * Default eloquent config.
     *
     * @var array
     */
    protected $config = [
        'driver' => 'mysql',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => '',
    ];

    /**
     * Alias between concrete config keys
     * and eloquent's
     *
     * @var array
     */
    protected $alias = [
        'server' => 'host',
    ];

    /**
     * Alias between concrete drivers
     * and eloquent's
     *
     * @var array
     */
    protected $driverAlias = [
        'c5_pdo_mysql' => 'mysql',
    ];

    /**
     * Default eloquent facade.
     *
     * @var string
     */
    protected $facade = 'DB';

    /**
     * Default eloquent config key.
     *
     * @var string
     */
    protected $eloquentConfig = 'eloquent';

    /**
     * Default concrete config key.
     *
     * @var string
     */
    protected $concreteConfig = 'concrete';

    /**
     * Registers the services provided by this provider.
     *
     * @return void
     */
    public function register()
    {
        $config = $this->getConfig();

        $manager = new DatabaseManager();
        $manager->addConnection($config);
        // Set the event dispatcher used by Eloquent models...
        $manager->setEventDispatcher(new Dispatcher());
        // Make this Capsule instance available globally via static methods...
        $manager->setAsGlobal();
        // Setup the Eloquent ORM...
        $manager->bootEloquent();

        $this->app->singleton('eloquent', function () use ($manager) {
            return $manager;
        });

        // register the facade
        ClassAliasList::getInstance()->registerMultiple([$this->facade => DatabaseManager::class]);

        Events::fire('eloquent_booted', Core::make('eloquent'));
    }

    /**
     * Get the database config.
     *
     * @return array
     * @throws Exception
     */
    protected function getConfig()
    {
        $connections = \Config::get('database.connections');

        if (! count($connections)) {
            throw new Exception(t('No database connections set.'));
        }

        $config = $connections[$this->eloquentConfig] ?: $connections[$this->concreteConfig];

        // fix the concrete config keys to meet the eloquent ones
        foreach ($this->alias as $key => $value) {
            if (isset($config[$key])) {
                $config[$value] = array_pull($config, $key);
            }
        }
        // fix the concrete driver to meet the eloquent one
        if (in_array($config['driver'], array_keys($this->driverAlias))) {
            $config['driver'] = $this->driverAlias[$config['driver']];
        }

        return array_merge($this->config, $config);
    }
}